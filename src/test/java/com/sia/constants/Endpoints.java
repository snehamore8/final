package com.sia.constants;

public class Endpoints {
    public static final String USERS_URL="/api/users?page=2";
    public static final String SINGLE_USER_URL="/api/users/2";
}
