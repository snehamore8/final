package com.sia.tests;

import com.sia.constants.Endpoints;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class SingleUserTest {
    @Test
    public void testSinggleUser(){
       String url= Endpoints.SINGLE_USER_URL;
given().contentType(ContentType.JSON).when().get(url).then().body("data.first_name",equalTo("Janet"));
    }

}
