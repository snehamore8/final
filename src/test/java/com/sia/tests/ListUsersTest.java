package com.sia.tests;

import com.sia.TestBase;
import com.sia.constants.Endpoints;
import io.restassured.http.ContentType;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
public class ListUsersTest extends TestBase{


    @Test
    public void testUsers(){
    //String url= Endpoints.USERS_URL;
    //given().contentType(ContentType.JSON).when().get(url).then().statusCode(200);
     //   (or)

        given().contentType(ContentType.JSON).when().get(Endpoints.USERS_URL).then().statusCode(200);
    }
    @Test
    public void testUserResponse(){
        String url= Endpoints.USERS_URL;
        given().contentType(ContentType.JSON).when().get(url).then().extract().response().prettyPrint();
    }
}
